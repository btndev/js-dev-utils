/* eslint-disable no-console */

const program = require('commander')

program
  .version('0.1.0')

program
  .command('validate [dir]')
  .option('--no-yarn-lock', 'Skip yarn.lock validation')
  .option('--no-package-json', 'Skip package.json validation')
  .description('validate project best practices')
  .action(require('./commands/validate'))

program
  .command('check-node [dir]')
  .description('Check node js version against packge.json engines')
  .action(require('./commands/checkNode'))

program.parse(process.argv)

if (process.argv.slice(2).length <= 0) {
  program.outputHelp()
}
