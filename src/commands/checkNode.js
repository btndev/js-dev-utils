/* eslint-disable no-console */
const chalk = require('chalk')

const ValidationError = require('../exceptions/ValidationError')
const validateNodeVersion = require('../helpers/validateNodeVersion')

function checkNode (dir = process.cwd()) {
  try {
    validateNodeVersion(dir)
    console.log(chalk.green('Node version looks ok!'))
  } catch (error) {
    if (error instanceof ValidationError) {
      console.error(chalk.red(error.message))
      process.exit(1) // eslint-disable-line unicorn/no-process-exit
    } else {
      throw error
    }
  }
}

module.exports = checkNode
