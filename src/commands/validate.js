/* eslint-disable no-console */
const chalk = require('chalk')

const isDirectory = require('../utils/isDirectory')
const ValidationError = require('../exceptions/ValidationError')
const validatePackageJson = require('../helpers/validatePackageJson')
const validateYarnLock = require('../helpers/validateYarnLock')

function shouldHandlePackageJson (path) {
  return path.endsWith('package.json') || isDirectory(path)
}

function shouldHandleYarnLock (path) {
  return path.match('yarn.lock') || isDirectory(path)
}

function validate (path = process.cwd(), { packageJson, yarnLock }) {
  try {
    if (shouldHandlePackageJson(path)) {
      if (packageJson) {
        validatePackageJson(path)
        console.log(chalk.green('package.json looks ok!'))
      } else {
        console.log(chalk.yellow('Skipping package.json validation'))
      }
    }

    if (shouldHandleYarnLock(path)) {
      if (yarnLock) {
        validateYarnLock(path)
        console.log(chalk.green('yarn.lock looks ok!'))
      } else {
        console.log(chalk.yellow('Skipping yarn.lock validation'))
      }
    }
  } catch (error) {
    if (error instanceof ValidationError) {
      console.error(chalk.red(error.message))
      process.exit(1) // eslint-disable-line unicorn/no-process-exit
    } else {
      throw error
    }
  }
}

module.exports = validate
