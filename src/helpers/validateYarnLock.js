const fs = require('fs')

const ValidationError = require('../exceptions/ValidationError')
const validateYarnDuplicates = require('../utils/validateYarnDuplicates')
const resolveFilePath = require('../utils/resolveFilePath')

function validateYarnLock (dir) {
  const path = resolveFilePath(dir, 'yarn.lock')

  if (!fs.existsSync(path)) {
    throw new ValidationError(`Missing ${path}, you should use "yarn" as your package manager`)
  }

  validateYarnDuplicates(path)
}

module.exports = validateYarnLock
