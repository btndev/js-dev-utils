const getPackageJson = require('./getPackageJson')
const validateRepoVer = require('../utils/validateRepoVer')
const ValidationError = require('../exceptions/ValidationError')

const semVerRegexp = /^([~^<>=]*)(([0-9]+)\.([0-9]+)\.([0-9]+))/
const isSemVer = (version) => (version.match(semVerRegexp))

function validateSemVer (version, dep) {
  const match = `${version}`.match(semVerRegexp)
  if (!match) {
    return
  }

  if (match[1] === '') {
    return
  }

  throw new ValidationError(`"${dep}" has invalid version (${version}) in package.json, install exact version of package for example: "${match[2]}"`)
}

function validateVersion (version, dep) {
  if (isSemVer(version)) {
    validateSemVer(version, dep)

    return
  }

  validateRepoVer(version, dep)
}

function validateVersions (versions) {
  const packages = Object.keys(versions)

  packages.forEach((dep) => {
    validateVersion(versions[dep], dep)
  })
}

function validateDependencies (packageJson) {
  const dependenciesFields = ['dependencies', 'devDependencies']

  dependenciesFields.forEach((field) => {
    if (field in packageJson) {
      validateVersions(packageJson[field])
    }
  })
}

function validatePackageJson (dir) {
  const packageJson = getPackageJson(dir)

  validateDependencies(packageJson)
}

module.exports = validatePackageJson
