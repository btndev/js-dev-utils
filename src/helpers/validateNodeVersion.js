const semver = require('semver')

const ValidationError = require('../exceptions/ValidationError')
const getPackageJson = require('./getPackageJson')

function validateNodeVersion (dir) {
  const packageJson = getPackageJson(dir)

  const { node: nodeVersion } = packageJson.engines

  if (!semver.satisfies(process.version, nodeVersion)) {
    throw new ValidationError(`
      Required node version ${nodeVersion} in package.json is not satisfied with current version ${process.version}.
      Install correct version using https://github.com/creationix/nvm or https://github.com/tj/n
    `)
  }
}

module.exports = validateNodeVersion
