const fs = require('fs')

const resolveFilePath = require('../utils/resolveFilePath')
const ValidationError = require('../exceptions/ValidationError')

module.exports = function getPackageJson (dir) {
  const path = resolveFilePath(dir, 'package.json')

  if (!fs.existsSync(path)) {
    throw new ValidationError(`${path} not exists`)
  }

  const packageJson = require(path) // eslint-disable-line import/no-dynamic-require, global-require

  return packageJson
}
