const { resolve, dirname, join } = require('path')

function resolveDir (dir) {
  return dir.match(/\.(json|lock)$/) ? dirname(dir) : dir
}

function resolveFilePath (dir, fileName) {
  return resolve(join(resolveDir(dir), fileName))
}

module.exports = resolveFilePath
