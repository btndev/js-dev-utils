const validateRepoVer = require('./validateRepoVer')

describe('validateRepoVer', () => {
  const createFixtures = (host, ext, version = '') => ([
    `${host}:test-account/test-repo${version}`,
    `git@${host}:test-account/test-repo${version}`,
    `git@${host}.${ext}:test-account/test-repo.git${version}`,
    `git+ssh://git@${host}.${ext}:test-account/test-repo.git${version}`,
  ])

  const commitHash = '#017a641e9671008c708c88810e38bf3696e77035'
  const semVer = '#1.0.0'

  const validUrls = [
    ...createFixtures('github', 'com', commitHash),
    ...createFixtures('github', 'com', semVer),
    ...createFixtures('bitbucket', 'com', commitHash),
    ...createFixtures('bitbucket', 'com', semVer),
  ]

  validUrls.forEach((validUrl) => {
    it(`should validate ${validUrl}`, () => {
      expect(() => {
        validateRepoVer(validUrl)
      }).not.toThrow()
    })
  })

  const invalidUrls = [
    ...createFixtures('github', 'com'),
    ...createFixtures('github', 'com'),
    ...createFixtures('bitbucket', 'com'),
    ...createFixtures('bitbucket', 'com'),
  ]

  invalidUrls.forEach((invalidUrl) => {
    it(`should not validate ${invalidUrl}`, () => {
      expect(() => {
        validateRepoVer(invalidUrl)
      }).toThrow()

      try {
        validateRepoVer(invalidUrl, 'test-repo')
      } catch (error) {
        expect(error).toMatchSnapshot()
      }
    })
  })
})
