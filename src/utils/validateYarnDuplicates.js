const fs = require('fs')
const semver = require('semver')
const { parse } = require('@yarnpkg/lockfile')

const ValidationError = require('../exceptions/ValidationError')

function extractPackages (yarnLockFile) {
  const re = /^(.*)@([^@]*?)$/
  const file = fs.readFileSync(yarnLockFile, 'utf8')
  const installedPackages = parse(file).object

  const packagesMap = Object.keys(installedPackages).reduce((acc, name) => {
    const packageConfig = installedPackages[name]
    const [, packageName, requestedVersion] = name.match(re)

    acc[packageName] = acc[packageName] || []
    acc[packageName].push({
      name,
      packageConfig,
      packageName,
      requestedVersion,
    })

    return acc
  }, {})

  return packagesMap
}

function extractVersions (packages) {
  return packages.map(({ packageConfig }) => (packageConfig.version))
}

function findDuplicates (allPackages) {
  const result = []

  Object.keys(allPackages).forEach((name) => {
    const packages = allPackages[name]
    const versions = extractVersions(packages)

    packages.forEach(({ packageConfig, requestedVersion }) => {
      const targetVersion = semver.maxSatisfying(versions, requestedVersion)
      if (targetVersion && targetVersion !== packageConfig.version) {
        result.push(`Package "${name}" wants ${requestedVersion} and could get ${targetVersion}, but got ${packageConfig.version}`)
      }
    })
  })

  return result
}

function validateYarnDuplicates (yarnLockFile) {
  const allPackages = extractPackages(yarnLockFile)
  const duplicates = findDuplicates(allPackages)

  if (duplicates.length > 0) {
    throw new ValidationError(duplicates.join('\n'))
  }
}

module.exports = validateYarnDuplicates
