const ValidationError = require('../exceptions/ValidationError')

const repoVersion = /([a-zA-Z0-9-]+\/[a-zA-Z0-9-]+)(\.git)?((#[a-zA-Z0-9]{40})|#[0-9]+\.[0-9]+\.[0-9]+)?/

function validateRepoVer (version, dep) {
  const match = version.match(repoVersion)

  if (!match) {
    throw new ValidationError(`"${dep}" has invalid version (${version}) in package.json`)
  } else if (!match[3]) {
    throw new ValidationError(`"${dep}" has invalid version (${version}) in package.json, install exact version using full commit hash or semver tag`)
  }
}

module.exports = validateRepoVer
