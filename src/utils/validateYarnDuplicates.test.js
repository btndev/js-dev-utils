const { resolve } = require('path')
const validateYarnDuplicates = require('./validateYarnDuplicates')

describe('validateYarnDuplicates', () => {
  const lockWithoutDuplicates = resolve(__dirname, './__fixtures__/yarn-lock-with-duplicates.lock')
  const lockWithDuplicates = resolve(__dirname, './__fixtures__/yarn-lock-without-duplicates.lock')
  const lockWithUnresolvableDuplicates = resolve(__dirname, './__fixtures__/yarn-lock-with-unresolvable-duplicates.lock')

  it('should throw validation exception for lock with duplicates', () => {
    expect(() => {
      validateYarnDuplicates(lockWithoutDuplicates)
    }).toThrowErrorMatchingSnapshot()
  })

  it('should not throw exception for lock without duplicates', () => {
    expect(() => {
      validateYarnDuplicates(lockWithDuplicates)
    }).not.toThrowError()
  })

  it('should not throw exception for lock with unresolvable duplicates', () => {
    expect(() => {
      validateYarnDuplicates(lockWithUnresolvableDuplicates)
    }).not.toThrowError()
  })
})
